#!/usr/bin/make -f

# Allow for silencing build process
ifeq (,$(filter terse,$(DEB_BUILD_OPTIONS)))
export DH_VERBOSE = 1
export PYBUILD_VERBOSE=1
else
export DH_QUIET = 1
endif

# Enable all hardening options
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND = -Wall -pedantic
DPKG_EXPORT_BUILDFLAGS = 1

# Needed for extracting build-related metadata
include /usr/share/dpkg/buildflags.mk
include /usr/share/dpkg/pkg-info.mk
include /usr/share/dpkg/architecture.mk

# Don't perform optimisation when we want to have all binary symbols
ifneq (,$(filter nostrip,$(DEB_BUILD_OPTIONS)))
DEB_CXXFLAGS_MAINT_STRIP=-O2
DEB_CXXFLAGS_MAINT_APPEND=-O0
endif

# Rename package to avoid potential conflict with psycopg2
export PYBUILD_NAME = psycopg
export PYBUILD_TEST_CUSTOM = 1
export PYBUILD_TEST_ARGS = $(CURDIR)/debian/run-tests {interpreter} {dir} {home_dir}

# Reproducible builds - build date is taken from debian/changelog
BUILD_DATE=$(shell LC_ALL=C date -u "+%B %d, %Y" -d "@$(SOURCE_DATE_EPOCH)")
SPHINXOPTS:=-D today=\"$(BUILD_DATE)\"
SPHINXOPTS+=-D html_last_updated_fmt=\"$(BUILD_DATE)\"

%:
	dh $@ --buildsystem=pybuild

# We need to build 3 different modules, from 3 different directories
override_dh_auto_clean-indep:
	PYBUILD_DIR=psycopg dh_auto_clean -- --name psycopg
	PYBUILD_DIR=psycopg_pool dh_auto_clean -- --name psycopg_pool
	rm -rf docs/_build

override_dh_auto_clean-arch:
	PYBUILD_DIR=psycopg_c dh_auto_clean -- --name psycopg_c

override_dh_auto_configure-indep:
	PYBUILD_DIR=psycopg dh_auto_configure -- --name psycopg
	PYBUILD_DIR=psycopg_pool dh_auto_configure -- --name psycopg_pool

override_dh_auto_configure-arch:
	PYBUILD_DIR=psycopg_c dh_auto_configure -- --name psycopg_c

override_dh_auto_build-indep:
	PYBUILD_DIR=psycopg dh_auto_build -- --name psycopg
	PYBUILD_DIR=psycopg_pool dh_auto_build -- --name psycopg_pool

override_dh_auto_build-arch:
	PYBUILD_DIR=psycopg_c dh_auto_build -- --name psycopg_c

override_dh_auto_install-indep:
	PYBUILD_DIR=psycopg dh_auto_install -- --name psycopg
	PYBUILD_DIR=psycopg_pool dh_auto_install -- --name psycopg_pool

override_dh_auto_install-arch:
	PYBUILD_DIR=psycopg_c dh_auto_install -- --name psycopg_c

execute_before_dh_installdocs-indep:
# Don't build documentation when required not to do it
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
# Documentation requires psycopg module to analyse existing types
	SPHINXOPTS="$(SPHINXOPTS)" pybuild --build -i python3 -s custom --build-args 'make -C {dir}/docs html'
endif

# Don't run tests when required not to do it
override_dh_auto_test-arch:
ifneq ($(DEB_HOST_ARCH),hurd-i386)
	PSYCOPG_IMPL=c dh_auto_test -- --name psycopg_c
endif

override_dh_auto_test-indep:
ifneq ($(DEB_HOST_ARCH),hurd-i386)
	PSYCOPG_IMPL=python dh_auto_test -- --name psycopg
endif
